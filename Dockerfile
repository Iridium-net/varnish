FROM varnish:latest
USER root
COPY --chown=root:root default.vcl.template /etc/varnish/default.vcl.template
COPY --chown=root:root hit-miss.vcl /etc/varnish/hit-miss.vcl
## Install start command
COPY --chown=root:root start.sh /usr/sbin/start
RUN chmod +x /usr/sbin/start && touch /etc/varnish/default.vcl && chmod -R 777 /etc/varnish

USER varnish
CMD ["start"]

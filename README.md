# Basic Varnish
This is a Varnish container addendum that adds a hand full of additional configuration variables into the container execution time layer to add a bit more portability without having needlesly to track config files.  From the get go I've just added two variables `$CID` and `$CPORT` which by default are set to `web` and `80` which means without setting anything this container should look for an upstream microservice named `web` and connect to its port on `80` to look for some http service to help cache.

an example of this systems operation could look something like this as a `docker-compose.yml` format. loosely defined as the following which should work out of the box so long as the host path exists.
```yaml
version: "3.9"

networks:
  default: {}

services:
  webshot:
    image: flowko1/website-shot:latest
    dns:
      - "176.103.130.130"
    # volumes:
    #   - /path/on/host:/usr/src/website-shot/screenshots
    networks:
      - default
  varnish:
    depends_on:
      - webshot
    restart: always
    image: codeberg.org/dowerent/varnish:latest
    environment:
      - VARNISH_SIZE=1G
      - VARNISH_HTTP_PORT=80
      - CONTAINER_HOSTNAME=webshot
      - CONTAINER_PORT=3000
    networks:
      - default
    ports:
      - target: 80
        published: 8081
        protocol: tcp
        mode: host
```
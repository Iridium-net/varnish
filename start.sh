#!/bin/bash
sed "s/CONTAINERHOSTNAME/${CONTAINER_HOSTNAME:=WEB}/" /etc/varnish/default.vcl.template > /etc/varnish/default.vcl
sed -i "s/port = \"\"/port = \"${CONTAINER_PORT:=80}\"/" default.vcl
cat /etc/varnish/default.vcl
/usr/local/bin/docker-varnish-entrypoint